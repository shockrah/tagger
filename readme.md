# Tagger - Tag based media storage

Store files of whatever kind and associate tags to those files.
The idea is to store once and search/read many times based on those tags with 
the intention of searching large amounts of media files easier.



## API Reference

### `GET /tags/list`

This basically just returns a JSON object with the shape: 

```
{
	"tags": []
}
```

### `PUT /file/tags`

Adds tags to a given file 

* [Required] filename : `string`

Filename in this context means the basename of the file which does not include the
directory path. For example: `basename(content/1234.png)` becomes `1234.png`.

* [Required] tags : `Array<string>`

Globally used tags are automatically updated if new tags are found

### `POST /file/upload`

Uploads a file and with the specified tags.

* [Required] name : `string`

* [Optional] tags : `Array<string>`

Globally used tags are automatically updated if new tags are found

* [Required] file : `file`

### `GET /tags/list`

Gets the full list of tags recorded in the database

* No parameters required

### `POST /tags/add`

* [Required] tags : `Array<string>`

### `DELETE /tags/remove`

Removes these tags from the global tags reference but does not remove tags 
from individual files.

* [Required] tags: `Array<string>`

Tags to remove 

## Configuration

* SERVICE\_PORT: default=1234

* OAUTH\_ENABLE: default=False

Enabling this adds on the restriction to use oauth with
your favorite service whatever that may be.

* API\_KEY\_ENABLE: default=False

Requires an API key to be used when accessing the API.
Can also be used in conjunction with OAuth 

* DEV\_ENV: default=False

Basically we means we don't run in debug mode
