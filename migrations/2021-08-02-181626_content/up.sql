CREATE TABLE IF NOT EXISTS content (
	name VARCHAR(256) NOT NULL,
	filepath VARCHAR(256) UNIQUE NOT NULL,
	time DATE NOT NULL,
	tags json,
	PRIMARY KEY(filepath)
);
