#!/bin/sh

_show_help() {
cat <<EOF
Usage: ./start-dev-service.sh [OPTION [ARG]]
	-h Shows this prompt
	-a Enables API feature
	-D [PASSWORD] pass in database password
		Not recommended to use this outisde of protected environments
	-d Enables developer mode
EOF
}

while getopts ":hdD:a" arg; do
	case ${arg} in 
		# Enable debug mode
		d) export DEV_ENV=yes;;
		# Pass in database password(usually for CI jobs)
		D) 
			echo $2
			export DATABASE_PASSWORD=$2;;
		# Enable API key feature(requires app pre-reqs[check readme.md])
		a) export API_KEY_enable=true;;
		h) 
			_show_help 
			exit 0
			;;
	esac
done

if [ -z $@ ];then 
	_show_help
fi

. bin/activate
python src/main.py
