#!/bin/sh

# This script basically sets up a simple docker based postgresql

docker run \
	--rm -d --name pg-dev \
	-e POSTGRES_PASSWORD=password \
	-v /home/shockrah/GitRepos/meme-db/data:/var/lib/postgresql/data \
	-p 5432:5432 postgres
