from db import Database
from auth import verify_jwt
from asyncpg import Record
from flask import Blueprint
from flask import jsonify
from flask import request
from werkzeug.datastructures import FileStorage
from json import dumps as json_str
from time  import time
from datetime import datetime
import subprocess as sub
import sys
import os

# Use the configured FILE_DIR 
FILE_DIR = './content/'

file_blueprint = Blueprint('file_blueprint', __name__, url_prefix='/file')

@verify_jwt
@file_blueprint.put('/tags')
async def append_tags_to_file():
    filename = request.args.get('filename')
    tags = request.args.getlist('tags')

    if filename is None:
        return 'wtf', 400

    query = 'SELECT filename, time, tags FROM content WHERE filename = $1'
    f = await Database.get_row(query, filename)
    if f is None:
        return '', 204
    # Assuming we have some kind of file
    tags = f['tags']
    print('Tags on file', tags)

    # If we have a file in the database then we can add some tags to it
    return 'adsf'


def filter_rows(dataset: list[Record], name: str, tags:list[str]) -> list[Record]:
    # Note this filter function is like... really b ad but im keeping it because
    # frankly asyncpg has some really weird parsing rules that I can't figure
    # out just yet so yea

    rows = []
    for row in dataset:
        row_name: str = row['name']
        row_tags: list[str] = row['tags']

        if name.lower() in row_name.lower():
            rows.append(row)
        for tag in tags:
            if tag in row_tags:
                rows.append(row)
                break

    return rows



def file_records_to_json(rows: list[Record]) -> list[dict]:
    parsed = []
    for row in rows:
        parsed.append({
            'name': row['name'],
            'filepath': row['filepath'],
            'time': row['time'],
            'tags': row['tags']
        })
    return parsed

@file_blueprint.get('/search')
async def search_files():
    tags = request.args.getlist('tags')
    name = request.args.get('name')
    index = request.args.get('offset')

    if len(tags) == 0 and name is None:
        return jsonify({'error': 'Tags or name must be provided'})

    
    query = 'SELECT name, filepath, time, tags FROM content'
    if index is None:
        query += ' ORDER BY time ASC LIMIT 1000'
        results = await Database.search_rows(query)
        remaining = filter_rows(results, name, tags)

        parsed = file_records_to_json(remaining)
        return jsonify({'files': parsed})
    else:
        query += 'ORDER BY time ASC OFFSET $1 LIMIT 1000'
        try:
            index = int(index)
            results = await Database.search_rows(query, index)
            remaining = filter_rows(results, name, tags)
            parsed = file_records_to_json(remaining)
            return jsonify({'files': parsed})
        except:
            return jsonify({'error': 'Index must be an integer'})



@verify_jwt
@file_blueprint.delete('/remove')
async def remove_file():
    filepath = request.args.get('filepath')
    if filepath is None:
        return jsonify({'error': 'filepath is a required argument'}), 400

    disk_rem, db_rem = await rollback_fileupload(name)
    ret = {
        'disk-removal': disk_rem,
        'db-rem': db_rem
    }

    if disk_rem is not True or db_rem is not True:
        http_code = 500
    return jsonify(ret)


async def rollback_fileupload(name: str):
    removals = [False, False]
    try:
        os.remove(name)
        removals[0] = True
    except FileNotFoundError:
        pass
    # Finally remove the entry from the postgres table
    query = 'DELETE FROM content WHERE = $1'
    try:
        await Database.delete(query, name)
        removals[1] = True
    except:
        pass





async def update_content(name: str, tags: list[str], filedata: FileStorage) -> dict:
    ret = {'file-upload': None, 'file-db-update': None}
    now = time()
    now_date = datetime.fromtimestamp(now)

    # First comes the extraction of the file extension
    dot_idx = filedata.filename.rfind('.')
    extension = filedata.filename[dot_idx:]
    if len(extension) < 2:
        extension = '.bin'

    # Finally we can save the file to disk
    name_on_disk = f'content/{int(now * 1000)}{extension}'
    try:
        filedata.save(name_on_disk)
        ret['file-upload'] = 'ok'
    except Exception as e:
        print(e, file=sys.stderr)
        ret['file-upload'] = 'failure'

    # We then update the content table and move on from there
    query = 'INSERT INTO content (name, filepath, time, tags) VALUES ($1, $2, $3, $4)'
    query_params = [
        name, name_on_disk, now_date, json_str(tags)
    ]
    db_result = await Database.insert_row(query, query_params)
    if db_result is None:
        ret['file-db-update'] = 'ok'
    else:
        ret['file-db-update'] = 'failure'
        print(db_result, file=sys.stderr)

    for result in ret.values():
        # If anything has failed we have to rollback the changes we made a second ago
        if result == 'failure':
            rollback_fileupload(name_on_disk)
            break

    return ret



@verify_jwt
@file_blueprint.post('/upload')
async def upload_file():
    # Query string params
    name = request.args.get('name')
    tags = request.args.getlist('tags')
    # FIle data itself
    file_ = request.files.get('file')

    # First we deal with parameters given to us
    if file_ is None or name is None:
        return jsonify({'error': 'Missing parameters'}), 400

    if len(tags) ==  0:
        tags = ['untagged']

    response = { 'stages': {} }
    # First we deal with the file uploading and content update
    response['stages'] = await update_content(name, tags, file_)

    http_code = 200
    for stage in response['stages']:
        if response['stages'][stage] == 'failure':
            http_code = 500

    if len(response['stages']) == 0:
        return jsonify(response), 500

    return jsonify(response), http_code

@file_blueprint.get('/all')
async def full_list():
    query = 'SELECT name, filepath, time, tags FROM content'
    result = await Database.search_rows(query)
    data = file_records_to_json(result)
    return jsonify({'files': data})



    


