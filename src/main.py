import logging as log
from flask import Flask
from os import getenv
import files
import auth
import tags

if __name__ == '__main__':
    '''
    Available configuration variables:
        SERVICE_PORT: default=1234
        DEV_ENV: default=False
            Basically we means we don't run in debug mode
        API_KEY_ENABLE: default=False
    '''
    # Must be set first as we rely heavily on this being initialized correctly
    log.basicConfig(level=log.INFO)

    # Check for outh enabled things
    api_key_check = getenv('API_KEY_ENABLE', None) is not None

    auth.API_CHECK_ENABLED = api_key_check

    # Discern what kind of env we're running in
    is_dev = getenv('DEV_ENV', None) is not None

    port = getenv('SERVICE_PORT')
    if port:
        try:
            port = int(port)
        except:
            log.error(f'UNABLE TO CONVERT {port} to an integer')
            exit(1)
    else:
        port = 1234

    app = Flask('tagger-service')

    # Setup our sub api's
    app.register_blueprint(files.file_blueprint)
    app.register_blueprint(tags.tags_blueprint)
    app.register_blueprint(auth.auth_blueprint)


    log.info("Starting API service")

    app.run(port=port, debug=is_dev)
