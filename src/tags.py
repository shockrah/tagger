from db import Database
from flask import Blueprint
from flask import request
from flask import jsonify
from asyncpg import Record
from auth import verify_jwt

import json
import sqlite3

tags_blueprint = Blueprint('tags_blueprint', __name__, url_prefix='/tags')
'''
Return value model:
    /list - 
'''

@tags_blueprint.get('/list')
async def list_tags():
    tags: list[Record] = await Database.search_rows('SELECT tag FROM tags')
    payload = []
    for record in tags:
        payload.append({key:value for (key, value) in record.items()})

    return jsonify({'tags': payload})


@verify_jwt
@tags_blueprint.post('/add')
async def add_tags():
    tags = request.args.getlist('tags')
    # First we have to collect all the tags currently available
    if len(tags) ==  0:
        return 'tags must be given', 400

    stmt = 'INSERT INTO tags (tag) VALUES ($1)'
    inserted = await Database.insert_rows(stmt, tags)
    return jsonify({'inserted': inserted})


@verify_jwt
@tags_blueprint.delete('/remove')
async def remove_tags():
    tags = request.args.getlist('tags')
    if len(tags) == 0:
        return 'Tags must be given', 400
    
    tags = await Database.delete_many('DELETE FROM tags WHERE tag = $1', tags)
    return jsonify({'removed': tags})


