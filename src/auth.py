from pymemcache.client import PooledClient
from base64 import b64encode
from flask import request
from flask import session
from flask import Blueprint
from functools import wraps
from flask import jsonify
from os import getenv
import logging as log
import json
import jwt

auth_blueprint = Blueprint('auth_blueprint', __name__, url_prefix='/auth')

client = PooledClient('127.0.0.1', max_pool_size=4)
API_CHECK_ENABLED = False

class Token:
    algorithm = 'HS256'
    key = getenv('JWT_TOKEN')

    @staticmethod
    def _ensure_secret():
        if key is None: 
            log.error('JWT_TOKEN is required for this feature')
            raise Exception('env:JWT_TOKEN not set')

    @staticmethod
    def generate_random_str(size=32) -> str:
        with open('/dev/urandom', 'rb') as random:
            raw = random.read(size)
        token = b64encode(raw).decode('utf-8')
        return token

    @classmethod
    def verify(cls):
        pass

    @classmethod
    def generate(cls):
        cls._ensure_secret()
        body = {'rng': cls.generate_random_str()}
        enc = jwt.encode(body, cls.key, algorithm=cls.algorithm)
        return enc


def verify_jwt(func):
    async def wrapper(*args, **kwargs):
        return await func(*args, **kwargs)
    return wrapper


def _check_api_key(func):
    async def api_check(*args, **kwargs):
        # Simple pass over this whole function and don't bother doing any lookups
        return jsonify({'asdf': False}), 400
        if not API_CHECK_ENABLED:
            log.info('Skipping API key check')
            return await func(*args, **kwargs)

        return await func(*args, **kwargs)

    return api_check


# Using memcached to store the keys for speed mostly
@_check_api_key
@auth_blueprint.post('/newtoken')
async def authorize():
    uid = request.args.get('id')
    if uid is None:
        return jsonify({'error': 'No id given'}), 400

    token = Token.generate_random_str()
    client.set(uid, token)
    return jsonify({'token': token})

@_check_api_key
@auth_blueprint.delete('/removetoken')
async def remove_key():
    uid = request.args.get('id')
    if uid is None:
        return jsonify({'error':'No id given'}), 400

    client.delete(uid)
    return jsonify({'status': 'ok'})




