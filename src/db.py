from asyncpg import Pool
from asyncpg import Connection
from asyncpg import connect
import asyncpg
import sys
import os




class Database:
    options = {
        'user': 'tagger',
        'command_timeout': 30,
        'password': os.environ.get('DATABASE_PASSWORD'),
        'database': 'taggerdb'
    }

    @classmethod
    async def search_rows(cls, stmt, *args):
        con = await connect(**cls.options)
        rows = []
        if len(args) == 0:
            rows = await con.fetch(stmt)
        else:
            rows = await con.fetch(stmt, *args)

        await con.close()
        return rows


    @classmethod
    async def insert_row(cls, stmt: str, values: list):
        con = await connect(**cls.options)
        ret = None
        try:
            await con.execute(stmt, *values)
        except asyncpg.exceptions.UniqueViolationError as common:
            print('Error ', common, file=sys.stderr)
            ret = common
        except Exception as fuck:
            print('Error', fuck, file=sys.stderr)
            ret = fuck
        finally:
            await con.close()
            return ret

    @classmethod
    async def insert_rows(cls, stmt: str, value_sets: list[list]):
        con = await connect(**cls.options)
        inserted = []
        try:
            for set in value_sets:
                await con.execute(stmt, set)
                inserted.append(set)
        except asyncpg.exceptions.UniqueViolationError as unique:
            print(unique, file=sys.stderr)
        finally:
            await con.close()
            return inserted

    @classmethod
    async def delete_many(cls, stmt: str, value_sets: list):
        con = await connect(**cls.options)
        removed = []
        try:
            for set in value_sets:
                await con.execute(stmt, set)
                removed.append(set)
        except Exception as e:
            print(e, file=sys.stderr)
        finally:
            await con.close()
            return removed

    @classmethod
    async def delete(cls, stmt: str, values: list):
        # Not much error handling here because DELETE is typically very quiet
        # therefore there's not much we do unless we do 999 queires to validate
        # prior which just creates race conditions
        con = await connect(**cls.options)
        try:
            # Kinda jank but ok
            if len(values) == 0 :
                await con.execute(stmt)
            else:
                await con.execute(stmt, *values)

        except Exception as e:
            print(f'Error in Database::delete', e, file=sys.stderr)
        finally:
            await con.close()



    @classmethod
    async def get_row(cls, stmt: str, *args):
        con = await connect(**cls.option)
        row = await con.fetchrow(stmt, args)
        await con.close()

        if row is None:
            return None
        else:
            return row



